#!/usr/bin/env python3

"""Play a sound file while showing bars with its amplitude"""

import sys

import sounddevice
import soundfile

# Period to compute mean value
period = 0.2

def mean(sound):
    total_left = 0
    total_right = 0
    for sample in sound:
        total_left += abs(sample[0])
        total_right += abs(sample[1])
    return int(total_left / len(sound)), int(total_right / len(sound))

def bars(mean_sample):
    stars = mean_sample[0] // 1000
    chars = (' ' * (33 - stars)) + ('*' * stars)
    stars = mean_sample[1] // 1000
    chars = chars + '|' + ('*' * stars) + (' ' * (33 - stars))
    return chars

def main():

    if len(sys.argv) != 2:
        print(f"Usage: {sys.argv[0]} <file_path>")
        exit(0)

    filename = sys.argv[1]

    print(f"Reading: {filename}")
    data, fs = soundfile.read(filename, dtype='int16')
    samples_period = int(period * fs)

    for nsample in range(0, len(data), samples_period):
        chunk = data[nsample : nsample + samples_period]
        sounddevice.wait()
        sounddevice.play(chunk, fs)
        mean_sample = mean(chunk)
        print(bars(mean_sample))
    sounddevice.wait()


if __name__ == '__main__':
    main()