"""Methods for storing sound, or playing it"""

import config
import sys

import sounddevice
import soundfile


def play(sound):
    sounddevice.play(sound, config.samples_second)
    sounddevice.wait()


def _mean(sound):
    total = 0
    for sample in sound:
        total += abs(sample)
    return int(total / len(sound))


def draw(sound, period: float):

    # El draw funcionaba antes del add  con todos los comentarios de abajo y el import sys
    if period > 0:
        samples_period = int(period * config.samples_second)

        '''if sum(sound)/len(sound) == config.min_amp:

            n1 = 1

        else:

            n1 = 0'''

        for nsample in range(0, len(sound), samples_period):

            chunk = sound[nsample: nsample + samples_period]
            mean_sample = _mean(chunk)
            stars = mean_sample // 1000
            print('*' * stars)

            '''if n1 == 1:

                print('*' * stars)

            elif sound[0] == config.min_amp:

                if chunk[0] == config.min_amp:

                    print()

                else:

                    print('*' * stars)

            else:

                print('*' * stars)

    else:

        sys.exit(f"Error, period({period}) must be positive")'''


def store(sound, path: str):

    soundfile.write(path, sound, config.samples_second)
