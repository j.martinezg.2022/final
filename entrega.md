# JORGE MARTÍNEZ GONZÁLEZ

REQUISITOS MÍNIMOS: 
    
    - Método square
    - Método store
    - Método ampli
    - Método reduce
    - Método extend
    - Extensión de source_sink.py
    - Creación de source_process_sink.py

REQUISITOS OPCIONALES:
    
    - Método readfile
    - Método round
    - Creación de sound_process.py
    - Creación de sound_processes.py
    - Creación de add
