"""Configuration and other constants"""

# samples per second
samples_second = 44100
# Maximum amplitude
max_amp = 32767
# Maximum amplitude
min_amp = -32767
# Pi
pi = 3.141592
