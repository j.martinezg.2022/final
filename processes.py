import sys

import sources
import config


def ampli(sound, factor: float):

    nsamples = len(sound)
    if factor > 0:

        for i in range(nsamples):

            n1 = int(sound[i] * factor)

            if n1 > config.max_amp:

                sound[i] = config.max_amp

            elif n1 < config.min_amp:
                sound[i] = config.min_amp

            else:

                sound[i] = n1

        return sound

    else:

        sys.exit(f"Error, factor({factor}) must be positive")


def reduce(sound, factor: int):

    if factor > 0:

        del sound[factor-1::factor]

        return sound
    else:

        sys.exit(f"Error, factor({factor}) must be positive")


def extend(sound, factor: int):

    nsamples = len(sound)

    n = 0
    if factor > 0:

        for i in range(factor, nsamples+1, factor):

            try:

                j = i+n
                sound.insert(j, (sound[j - 1] + sound[j]) // 2)
                n += 1

            except:

                j = i + n
                sound.insert(j, sound[j - 1])
                n += 1

        return sound

    else:

        sys.exit(f"Error, factor({factor}) must be positive")


def round(sound, samples: int):

    nsamples = len(sound)

    if samples > 0:

        for i in range(0, nsamples):
            if i < samples:

                a = 0
                b = samples + i
                c = 0

                while b >= 0:

                    a += sound[b]
                    b -= 1
                    c += 1

                a = a // c
                sound[i] = a

            elif i >= samples:

                if i >= nsamples - samples:

                    a = 0
                    b = i - samples
                    c = 0

                    while b < nsamples:
                        a += sound[b]
                        b += 1
                        c += 1

                    a = a // c
                    sound[i] = a

                else:

                    a = 0

                    for j in range(i - samples, i+samples+1):

                        a += sound[j]

                    a = a // ((samples * 2) + 1)
                    sound[i] = a

    else:

        sys.exit(f"Error, samples({samples}) must be positive")


def add(sound, dur: float, source, source_args):

    if source == 'sin':
        sound_add = sources.sin(duration=dur, freq=float(source_args))

    elif source == 'constant':
        sound_add = sources.constant(duration=dur, positive=bool(source_args))

    elif source == 'square':
        sound_add = sources.square(duration=dur, freq=float(source_args))

    elif source == 'read':
        sound_add = sources.readfile(duration=dur, path=source_args)

    else:
        sys.exit("Unknown source")

    return sound + sound_add
